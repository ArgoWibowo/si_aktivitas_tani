<?php
  
  $host="localhost";
  $user="root";
  $pass="";
  $dbname="iais_ukdw";
  
  $dbcon = new PDO("mysql:host={$host};dbname={$dbname}",$user,$pass);
  
  if($_POST) 
  {
      $id_lahan     = strip_tags($_POST['id_lahan']);
      
   $stmt=$dbcon->prepare("SELECT ID_Lahan FROM master_peta_lahan WHERE ID_Lahan=:id_lahan");
   $stmt->execute(array(':id_lahan'=>$id_lahan));
   $count=$stmt->rowCount();
      
   if($count>0)
   {
    echo "<span style='color:red;'>Maaf ID lahan Sudah Terpakai !!!</span>";
   }
   else
   {
    echo "<span style='color:green;'>ID lahan Tersedia</span>";
   }
  }
?>