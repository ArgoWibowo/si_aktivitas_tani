<?php
//include("koneksi.php");
$pro = $_GET['pro'];
$tahun = $_GET['tahun'];
$aktivitas = $_GET['aktivitas'];
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
    </head><body>
 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                                <div style="margin-bottom:15px;" align="right">
                                    
                                </div>
                            </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
          </div>
    </nav>

     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php                                                         
                                            mysql_connect("localhost","root","") or die(mysql_error());
                                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                            $query1= mysql_query("SELECT * FROM master_aktivitas where ID_Aktivitas = '$aktivitas';");
                                            $brs = mysql_fetch_assoc($query1);
                                            ?>
                                            <h2>Summary Aktivitas <?php echo $brs['Nama_Aktivitas']?> di <?php echo $pro?> <?php echo $tahun?></h2>
                                             <div class="col-md-2">
                                                <a style="font-size:20px". onclick="window.history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</a>
                                            </div>
                                            <div class="col-md-2">
                                                <a style="font-size:20px". href="Daftar laporan aktivitas pertanian provinsi.php?pro=<?php echo $pro;?>&tahun=<?php echo $tahun;?>&aktivitas=<?php echo $aktivitas;?>"><span class="glyphicon glyphicon-print"></span> Cetak</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table" style="width: 500px;">
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Tanaman</th>
                                   <th>Jumlah</th>
                              </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from master_spesies_tanaman");
                                                $no = 1;
                                                $total=0;
                                                while($tanaman = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $tanaman['Nama_Tanaman']?>
                                                    <?php
                                                    $tanam =  $tanaman['ID_Spesies'];
                                                    ?>    
                                                    </td>
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT count(*) as jumlah FROM master_aktifitas_spesies Inner JOIN trans_aktivitas_pertanian ON master_aktifitas_spesies.ID_aktifitas_spesies = trans_aktivitas_pertanian.ID_aktifitas_spesies inner join master_petani on trans_aktivitas_pertanian.ID_Petani = master_petani.ID_User inner join master_spesies_tanaman on master_aktifitas_spesies.ID_Spesies = master_spesies_tanaman.ID_Spesies inner join master_aktivitas on master_aktifitas_spesies.Id_Aktivitas = master_aktivitas.Id_Aktivitas where Provinsi = '$pro' AND Tahun_Aktivitas = '$tahun' and master_aktivitas.Id_Aktivitas = '$aktivitas' and master_spesies_tanaman.ID_Spesies = '$tanam'  group by Nama_Tanaman order by Nama_Tanaman");
                                                   
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    echo $data2['jumlah'];
                                                    $total=$total+$data2['jumlah'];
                                                    ?>
    
                                                    </td>
                                                </tr>
                                                <?php
                                                
                                                }

                                                ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo $total;
                                                    ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            
                  
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--end konten-->
                     </div>
                </div>
            </div>
         </div>
          <script src="css/side_menu.js"></script>
        <footer class="navbar navbar-default navbar-fixed-bottom">
            <div class="container-fluid">
                <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
            </div>
        </footer>                         
</body></html>