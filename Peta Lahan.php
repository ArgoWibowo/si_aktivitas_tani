<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
                <script type="text/javascript" src="jquery-1.11.3-jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function()
        {    
         $("#id_lahan").keyup(function()
         {  
          var id_lahan = $(this).val(); 
          
          if(id_lahan.length > 2)
          {  
           $("#result").html('checking...');
           $.ajax({
    
            type : 'POST',
            url  : 'cek_id_lahan.php',
            data : $(this).serialize(),
            success : function(data)
                {
                    //alert(data); 
                    $("#result").html(data);
                   }
            });
            return false;
           
          }
          else
          {
           $("#result").html('');
          }
         });
         
        });
</script>
    </head><body>
<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">HAI! <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li><a href="tutup_session.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>

     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Tanaman</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="daftar_morfologi.php">Morfologi Tanaman</a></li>
                        <li><a href="Daftar Tanaman.php">Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam.php">Kalender Tanam</a></li>
                        <li><a href="Daftar Peta Lahan.php">Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Data Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Aktivitas.php">Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php">Aktivitas Tanaman</a></li>
                
                    </ul>
                </li>
               
            </ul>
        </div><!-- /#sidebar-wrapper -->
        
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                        <div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1>Peta Lahan</h1>
                                        <a style="font-size:20px" href="Daftar Peta Lahan.php"><span class="glyphicon glyphicon-list"></span> Daftar Peta Lahan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal" role="form" action="input_peta_lahan.php" method="post">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">ID Lahan*</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="id_lahan" id="id_lahan" placeholder="ID Lahan" maxlength="10" required>
                                                    <span id="result"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label  class="control-label">Koordinat X</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="koordinat_x" placeholder="Koordinat X" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Koordinat Y</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="koordinat_y" placeholder="Koordinat Y" required>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Provinsi</label>
                                                </div>
                                                <div class="col-sm-4">
                                                      <select class="form-control" id="prov" name="provinsi" required>
                                                          <option value="" disabled selected>Pilih Provinsi</option>
                                                          <?php
                                                          //include ("koneksi.php");
                                                          mysql_connect("localhost","root","") or die(mysql_error());
                                                          mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                          $query= mysql_query('SELECT * FROM Provinsi;');
                                                          if (mysql_num_rows($query) != 0){ 
                                                              while($brs = mysql_fetch_assoc($query)){ 
                                                                  echo '<option value="'.$brs['Nama_Provinsi'].'">'.$brs['Nama_Provinsi'].'</option>';
                                                              }
                                                          }

                                                          ?>
                                                      </select>
                                                  </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Kabupaten</label>
                                                </div>
                                                 <div class="col-sm-4">
                                                      <select class="form-control" id="txtHint" name="kabupaten" required>
                                                          <option value="" disabled selected>Pilih Kabupaten</option>
                                                         
                                                      </select>
                                                  </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Kecamatan</label>
                                                </div>
                                                <div class="col-sm-4">
                                                      <select class="form-control" id="txtHintkec" name="kecamatan" required>
                                                          <option value="" disabled selected>Pilih Kecamatan</option>
                                                         
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Desa</label>
                                                </div>
                                                <div class="col-sm-4">
                                                      <select class="form-control" id="txtHintkel" name="desa" required>
                                                          <option value="" disabled selected>Pilih Kelurahan</option>
                                                         
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Bulan Awal</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name="bulan">
                                                        <option value="1">Januari</option>
                                                        <option value="2">Februari</option>
                                                        <option value="3">Maret</option>
                                                        <option value="4">April</option>
                                                        <option value="5">Mei</option>
                                                        <option value="6">Juni</option>
                                                        <option value="7">Juli</option>
                                                        <option value="8">Agustus</option>
                                                        <option value="9">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Bulan Akhir</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name="bulan_akhir">
                                                        <option value="1">Januari</option>
                                                        <option value="2">Februari</option>
                                                        <option value="3">Maret</option>
                                                        <option value="4">April</option>
                                                        <option value="5">Mei</option>
                                                        <option value="6">Juni</option>
                                                        <option value="7">Juli</option>
                                                        <option value="8">Agustus</option>
                                                        <option value="9">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Spesies</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name="id_spesies" required>
                                                        <option value="" disabled selected>Pilih Tanaman</option>
                                                        <?php
                                                        //include ("koneksi.php");
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query('SELECT * FROM master_spesies_tanaman;');
                                                        if (mysql_num_rows($query) != 0){ 
                                                            while($brs = mysql_fetch_assoc($query)){ 
                                                                echo '<option value="'.$brs['ID_Spesies'].'">'.$brs['Nama_Tanaman'].'</option>';
                                                            }
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <input class="btn btn-primary btn-lg" type="submit" value="submit">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
             </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
        <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
    

</body></html>
<script>

 $(document).ready(function(){
    $("#prov").change(function(){
        showHint($("#prov").val());
    });
});                                             

function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                $("#txtHint").empty();
                $("#txtHint").append("<option>------ Kabupaten---------</option>");
                for (var i = 0; i < hasil.length - 1; ++i) {
                    $("#txtHint").append(hasil[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkab.php?q=" + str, true);
        xmlhttp.send();
    }
}
</script>
<script>

 $(document).ready(function(){
    $("#txtHint").change(function(){
        showHintkec($("#txtHint").val());
    });
});                                             

function showHintkec(strkec) {
    if (strkec.length == 0) { 
        document.getElementById("txtHintkec").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasilkec = this.responseText.split(",");
                $("#txtHintkec").empty();
                $("#txtHintkec").append("<option>------ Kecamatan---------</option>");
                for (var i = 0; i < hasilkec.length - 1; ++i) {
                    $("#txtHintkec").append(hasilkec[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkec.php?qkec=" + strkec, true);
        xmlhttp.send();
    }
}
</script>

<script>

 $(document).ready(function(){
    $("#txtHintkec").change(function(){
        showHintkel($("#txtHintkec").val());
    });
});                                             

function showHintkel(strkel) {
    if (strkel.length == 0) { 
        document.getElementById("txtHintkel").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasilkel = this.responseText.split(",");
                $("#txtHintkel").empty();
                $("#txtHintkel").append("<option>------ Kelurahan/Desa---------</option>");
                for (var i = 0; i < hasilkel.length - 1; ++i) {
                    $("#txtHintkel").append(hasilkel[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkel.php?qkel=" + strkel, true);
        xmlhttp.send();
    }
}
</script>