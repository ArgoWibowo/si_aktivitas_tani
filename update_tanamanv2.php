<?php
//include("koneksi.php");
$koneksi = mysqli_connect("localhost","root","","iais_ukdw") or die (mysqli_error());
$id_spesies = $_POST['id_tanaman'];
$nama_tanaman = $_POST['nama_tanaman'];
$nama_latin = $_POST['nama_latin'];
$habitat = $_POST['habitat'];
$jenis_tanaman = $_POST['jenis_tanaman'];
$masa_tanam = $_POST['masa_tanam'];
$akar = $_POST['akar'];
$batang = $_POST['batang'];
$daun = $_POST['daun'];
$buah = $_POST['buah'];
$biji = $_POST['biji'];
$perkembangbiakan = $_POST['perkembangbiakan'];

$foto1name = basename($_FILES['foto1_tanaman']['name']);
$foto1extension = pathinfo($foto1name, PATHINFO_EXTENSION);
$target_dir_1 = $_SERVER["DOCUMENT_ROOT"]. "/skripsi/images/foto1/";
$target_file_1 = $target_dir_1 . $id_spesies . "." . $foto1extension;
$imageFileType1 = pathinfo($target_file_1,PATHINFO_EXTENSION);
$namaimage1 = $id_spesies . "." . $foto1extension;

$foto2name = basename($_FILES['foto2_tanaman']['name']);
$foto2extension = pathinfo($foto2name, PATHINFO_EXTENSION);
$target_dir_2 = $_SERVER["DOCUMENT_ROOT"]. "/skripsi/images/foto2/";
$target_file_2 = $target_dir_2 . $id_spesies . "." . $foto2extension;
$imageFileType2 = pathinfo($target_file_2,PATHINFO_EXTENSION);
$namaimage2 = $id_spesies . "." . $foto2extension;

$uploadok = 1;
$iklim = $_POST['iklim'];
$jenis_tanah = $_POST['jenis_tanah'];
$kelembaban = $_POST['kelembaban'];
$id_morfologi = $_POST['id_morfologi'];


if($_FILES["foto1_tanaman"]["name"]=="" and $_FILES["foto2_tanaman"]["name"]==""){
  if($statement = mysqli_prepare($koneksi, "UPDATE master_spesies_tanaman SET Jenis_Tanaman='$jenis_tanaman', Nama_Tanaman='$nama_tanaman',
Nama_Latin='$nama_latin',Habitat='$habitat',Masa_Tanam=$masa_tanam,Akar='$akar',Batang='$batang', 
Daun='$daun',Buah='$buah',Biji='$biji',Perkembangbiakan='$perkembangbiakan',
Iklim='$iklim',Jenis_Tanah='$jenis_tanah',Kelembaban='$kelembaban',ID_Morfologi='$id_morfologi' WHERE ID_Spesies='$id_spesies'"))
  {

    if (mysqli_stmt_execute($statement))
    {

        header("Location:Daftar Tanaman.php?pesan=Data telah berhasil diubah...&jenis=text-success");
    }
    else
    {

       header("Location:Daftar Tanaman.php?pesan=Data berhasil diubah...&jenis=text-error");
    }

    mysqli_stmt_close($statement);
  }
} 
  elseif($_FILES["foto1_tanaman"]["name"]!="" and $_FILES["foto2_tanaman"]["name"]!="" ){
  if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["foto1_tanaman"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadok = 1;
    } else {
        echo "File is not an image.";
        header("Location:Daftar Tanaman.php?pesan=tidak berhasil diubah...&jenis=text-error");
        $uploadok = 0;
    }
 }
if(isset($_POST['submit'])){
	$check = getimagesize($foto1_tanaman_tmp);
	if($check !== false){
		echo "file is an image -".$check["mime"].".";
		$uploadok = 1;
	} else {
		echo "file bukan gambar";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
		$uploadok =0;
	}
}


	if ($_FILES["foto1_tanaman"]['size'] > 2000000 ){
		echo "Maaf Gambar terlalu besar.";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan foto terlalu besar (maks. 2mb)...&jenis=text-error");
		$uploadok =0;	
	}
	if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
		&& $imageFileType1 !="gif"){
		echo "Maaf hanya gambar dengan format JPG, JPEG, PNG & GIF files yang diperbolehkan";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
		$uploadok =0;
	}
	if($uploadok == 0){
		echo "maaf terjadi kesalahan gambar tidak terupload";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
	} else

	{
		if(move_uploaded_file($_FILES["foto1_tanaman"]["tmp_name"],$target_file_1))
		{


		if(isset($_POST["submit"])){
		$check = getimagesize($_FILES["foto2_tanaman"]["tmp_name"]);
		if($check!== false){
			echo "file is an image -".$check["mime"].".";
			$uploadok = 1;
		} else {
			echo "file bukan gambar";
			header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
			$uploadok =0;
		}	
	}


	if ($_FILES["foto2_tanaman"]['size'] > 2000000){
		echo "Maaf Gambar terlalu besar.";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan foto terlalu besar (maks. 2mb)...&jenis=text-error");
		$uploadok =0;	
	}
	if($imageFileType2 != "jpg" && $imageFileType1 != "png" && $imageFileType2 != "jpeg"
		&& $imageFileType2 !="gif"){
		echo "Maaf hanya gambar dengan format JPG, JPEG, PNG & GIF files yang diperbolehkan";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
		$uploadok =0;
	}
	if ($uploadok == 0){
		echo "maaf terjadi kesalahan gambar tidak terupload";
		header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
		} else 

		{
		if (move_uploaded_file($_FILES["foto2_tanaman"]["tmp_name"], $target_file_2))
			{
			if($statement = mysqli_prepare($koneksi, "UPDATE master_spesies_tanaman SET Jenis_Tanaman='$jenis_tanaman', Nama_Tanaman='$nama_tanaman',
			Nama_Latin='$nama_latin',Habitat='$habitat',Masa_Tanam=$masa_tanam,Akar='$akar',Batang='$batang', 
			Daun='$daun',Buah='$buah',Biji='$biji',Perkembangbiakan='$perkembangbiakan',
			Iklim='$iklim',Jenis_Tanah='$jenis_tanah',Kelembaban='$kelembaban',ID_Morfologi='$id_morfologi' ,foto1='$namaimage1', foto2='$namaimage2' WHERE ID_Spesies='$id_spesies'"))

			if(mysqli_stmt_execute($statement))
			{
			header("Location: Daftar Tanaman.php?pesan=Data berhasil ditambahkan...&jenis=text-success");
				echo "berhasil";	
			}
			else
			{
			header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil ditambahkan...&jenis=text-error");
			echo "gagal";
			}
			mysqli_stmt_close($statement);	
			}	
		}

		}
	}
}
elseif($_FILES["foto1_tanaman"]["name"]!="" and $_FILES["foto2_tanaman"]["name"]=="" ){
  if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["foto1_tanaman"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadok = 1;
    } else {
        echo "File is not an image.";
        header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil diedit...&jenis=text-error");
        $uploadok = 0;
    }
 }
    // Check if file already exists

    // Check file size
    if ($_FILES["foto1_tanaman"]["size"] > 2000000) {
        echo "Maaf Gambar / Foto Terlalu Besar";
        header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil diedit foto terlalu besar (maks. 2mb)...&jenis=text-error");
        $uploadok = 0;
    }
    // Allow certain file formats
    if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
    && $imageFileType1 != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      	header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil diedit...&jenis=text-error");
        $uploadok = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadok == 0) {
        echo "Maaf File Tidak Terupload";
        header("Location: Daftar Tanaman.php?pesan=Data tidak berhasil diedit...&jenis=text-error");
    // if everything is ok, try to upload file
    }else 

    {
    if (move_uploaded_file($_FILES["foto1_tanaman"]["tmp_name"], $target_file_1))
        {
           if($statement = mysqli_prepare($koneksi, "UPDATE master_spesies_tanaman SET Jenis_Tanaman='$jenis_tanaman', Nama_Tanaman='$nama_tanaman',
			Nama_Latin='$nama_latin',Habitat='$habitat',Masa_Tanam=$masa_tanam,Akar='$akar',Batang='$batang', 
			Daun='$daun',Buah='$buah',Biji='$biji',Perkembangbiakan='$perkembangbiakan',
			Iklim='$iklim',Jenis_Tanah='$jenis_tanah',Kelembaban='$kelembaban',ID_Morfologi='$id_morfologi' ,foto1='$namaimage1' WHERE ID_Spesies='$id_spesies'"))
            {
            if(mysqli_stmt_execute($statement))
              {
                header("Location: Daftar Tanaman.php?pesan=berhasil ditambahkan...&jenis=text-success");
              	echo "berhasil";
              }
              else
              {
                header("Location: Daftar Tanaman.php?pesan=tidak berhasil ditambahkan...&jenis=text-error");
              	echo "tidak";
              }

              mysqli_stmt_close($statement);  
            }
              
         }
              else
              {
                header("Location: Daftar Tanaman.php.php?pesan=tidak berhasil ditambahkan...&jenis=text-error");
              	echo "gak tau";
              }
    }    
  }

  elseif($_FILES["foto1_tanaman"]["name"]=="" and $_FILES["foto2_tanaman"]["name"]!="" ){
  if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["foto2_tanaman"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadok = 1;
    } else {
        echo "File is not an image.";
        header("Location: Daftar Tanaman.php?pesan=tidak berhasil diedit...&jenis=text-error");
        $uploadok = 0;
    }
 }
    // Check if file already exists

    // Check file size
    if ($_FILES["foto2_tanaman"]["size"] > 2000000) {
        echo "Maaf Gambar / Foto Terlalu Besar";
        header("Location: Daftar Tanaman.php?pesan=tidak berhasil diedit foto terlalu besar (maks. 2mb)...&jenis=text-error");
        $uploadok = 0;
    }
    // Allow certain file formats
    if($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg"
    && $imageFileType2 != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      	header("Location: Daftar Tanaman.php?pesan=tidak berhasil diedit...&jenis=text-error");
        $uploadok = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadok == 0) {
        echo "Maaf File Tidak Terupload";
        header("Location: Daftar Tanaman.php?pesan=tidak berhasil diedit...&jenis=text-error");
    // if everything is ok, try to upload file
    }else 

    {
    if (move_uploaded_file($_FILES["foto2_tanaman"]["tmp_name"], $target_file_2))
        {
           if($statement = mysqli_prepare($koneksi, "UPDATE master_spesies_tanaman SET Jenis_Tanaman='$jenis_tanaman', Nama_Tanaman='$nama_tanaman',
			Nama_Latin='$nama_latin',Habitat='$habitat',Masa_Tanam=$masa_tanam,Akar='$akar',Batang='$batang', 
			Daun='$daun',Buah='$buah',Biji='$biji',Perkembangbiakan='$perkembangbiakan',
			Iklim='$iklim',Jenis_Tanah='$jenis_tanah',Kelembaban='$kelembaban',ID_Morfologi='$id_morfologi' ,foto2='$namaimage2' WHERE ID_Spesies='$id_spesies'"))
            {
            if(mysqli_stmt_execute($statement))
              {
                header("Location: Daftar Tanaman.php?pesan=telah berhasil ditambahkan...&jenis=text-success");
              	echo "berhasil";
              }
              else
              {
                header("Location: Daftar Tanaman.php?pesan=tidak berhasil ditambahkan...&jenis=text-error");
              	echo "tidak";
              }

              mysqli_stmt_close($statement);  
            }
              
         }
              else
              {
                header("Location: Daftar Tanaman.php?pesan=tidak berhasil ditambahkan...&jenis=text-error");
              	echo "gak tau";
              }
    }    
  }
?>