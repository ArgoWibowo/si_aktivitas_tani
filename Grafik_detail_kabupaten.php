<?php
$id_spesies= $_GET['spesies'];
$tahun = $_GET['tahun'];
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
         <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Berdasarkan Kabupaten'
        },
        subtitle: {
            text: 'Per Tanggal <?php echo date('d/m/Y'); ?>'
        },
        xAxis: {
            categories: [
            <?php
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT master_petani.Kabupaten as nama, sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$id_spesies' and Tahun_Panen='$tahun' And Provinsi = '$_GET[provinsi]' GROUP BY Kabupaten ORDER BY Kabupaten");
                while($datakabupaten = mysqli_fetch_array($hasil)){
            ?>
                '<?php echo $datakabupaten['nama']; ?>',
            <?php } ?>
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Panen (Kuintal)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">: </td>' +
                '<td style="padding:0;font-size:12px">{point.y:.f} Kuintal</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {

                borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                }
            }, 
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            //window.open(this.options.url);
                            document.location = this.options.url;
                        }
                    }
                }
            }
        },
        legend: { symbolHeight: '0px' },
        series: [{
            name: 'Nama Provinsi',
            data: [
            <?php
                $arrcolor = ['#FF0000','#00FF00','#0000FF','#FFFF00','#FF00FF','#00FFFF','#F00F00','#FFCC00'];
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT master_petani.Kabupaten as nama, sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$id_spesies' and Tahun_Panen='$tahun' And Provinsi = '$_GET[provinsi]' GROUP BY Kabupaten ORDER BY Kabupaten");
                $i=0;
                while($datakabupaten = mysqli_fetch_array($hasil)){
            ?>
                {y:<?php echo $datakabupaten['jumlah']; ?>,url:'Grafik_detail_kecamatan.php?Kabupaten=<?php echo $datakabupaten['nama'];?>&spesies=<?php echo $id_spesies;?>&tahun=<?php echo $tahun;?>'}, 
            <?php 
                $i++;
                } ?>
            ]

        }]
    });
});
        </script>
    </head><body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    
<body>
    
            <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php                                                         
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query= mysql_query("SELECT * FROM master_spesies_tanaman where ID_Spesies = '$_GET[spesies]';");
                            $brsnama = mysql_fetch_assoc($query);
                            ?>
                        <h1>Informasi Panen Tanaman <?php echo $brsnama['Nama_Tanaman']?> <?php echo $tahun?></h1>
                       <div class="col-md-2">
                          <a style="font-size:20px". onclick="window.history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</a>
                        </div>
                    </div>
                    <div style="margin-bottom:15px;" align="center">
                     
                    </div>
                </div>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                              
                        <script src="css/highcharts.js"></script>
                         <script src="css/highcharts/modules/exporting.js"></script>
                        

                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-8">  
                            </div>
                            <div class="col-md-4 text-left">
                                <a class="btn btn-lg btn-info" href="Summary_grafik_detail_kabupaten.php?spesies=<?php echo $id_spesies;?>&provinsi=<?php echo $_GET['provinsi']?>&tahun=<?php echo $tahun;?>"> Summary</a>
                            </div>
                        </div>
                        

                            <div class="col-md-6">
                           
                        </div>

                </div>
                <script src="css/side_menu.js"></script>

            </div>
</script>
</body>
</html>