<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar-tanaman.css" rel="stylesheet">
    </head><body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    
     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Pilihan Grafik Hasil Panen</h1>
                                            <a href="Grafik_detail_panen.php">Pilihan<a> >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <form role="form" action="Grafik_detail_provinsi.php" enctype="multipart/form-data" method="post">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <label class="control-label">Nama Tanaman</label>
                                                        </div>    
                                                        <div class="col-sm-4">
                                                            <select class="form-control" name="tanaman" required>
                                                                        <option value="" disabled selected>Pilih Tanaman</option>
                                                                        <?php
                                                                        //include ("koneksi.php");
                                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                                        $query= mysql_query('SELECT * FROM master_spesies_tanaman;');
                                                                        if (mysql_num_rows($query) != 0){ 
                                                                            while($brs = mysql_fetch_assoc($query)){

                                                                            echo '<option value="'.$brs['ID_Spesies'].'">'.$brs['Nama_Tanaman'].'</option>';
                                                                            }
                                                                        }

                                                                        ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">    
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <label class="control-label">Tahun Panen</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="number" min="2000" max="3000" value="2016" class="form-control" name="tahun">
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <input class="btn btn-primary btn-lg" type="submit" value="submit">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--konten-->
                    </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
</body></html>