<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                                <div style="margin-bottom:15px;" align="right">
                                    <form action="cari_hasil_panen-lihat.php" method="post">
                                        <input type="text" name="input_cari_hasil_panen" placeholder="Cari " style="width:250px;color:black;" />
                                        <input type="submit" name="cari" value="Cari" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                                    </form>
                                </div>
                            </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
          </div>
    </nav>

     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Hasil Panen</h1>
                                            <div class="col-md-2">
                                                <a style="font-size:20px". href="Daftar laporan hasil panen lihat.php"><span class="glyphicon glyphicon-print"></span> Cetak</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form role="form" action="Daftar_hasil_panen_lihat_filter.php" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3>Sort</h3>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Urutkan berdasarkan :</label>
                                                    <select class="form-control" name="berdasar">
                                                        <option value ="Nama_Petani">Nama Petani</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Tahun_Panen">Tahun Panen</option>
                                                        <option value ="Bulan_Panen">Bulan Panen</option>
                                                        <option value ="Periode_Panen">Periode Panen</option>
                                                        <option value ="Nama_Produk">Nama Produk Panen</option>
                                                        <option value ="Luas_Lahan">Luas Lahan</option>           
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <label class="control-label">dari</label>
                                                    <select class="form-control" name="urut">
                                                        <option value ="ASC">A-Z</option>
                                                        <option value ="DESC">Z-A</option>        
                                                    </select>
                                        </div>
                                        <div class ="col-md-4"> 
                                            </br>
                                          <div class ="row">   
                                            <div class ="col-md-2">  
                                            <input class="btn btn-primary btn-md" type="submit" value="Sort">
                                            </div>
                                            
                                           </div>                       
                                        </div>                                       
                                    </div>               
                                </div>
                                
                            </form>
                            <form role="form" action="Daftar_hasil_panen_lihat_filter_2.php" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3>Filter</h3>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Filter berdasarkan :</label>
                                                    <select class="form-control" name="filter1">
                                                        <option value ="Nama_Petani">Nama Petani</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Tahun_Panen">Tahun Panen</option>
                                                        <option value ="Bulan_Panen">Bulan Panen</option>
                                                        <option value ="Periode_Panen">Periode Panen</option>
                                                        <option value ="Nama_Produk">Nama Produk Panen</option>
                                                        <option value ="Luas_Lahan">Luas Lahan</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Filter</label>
                                                    <input class="form-control" name="isi1" placeholder=" " type="text" required>
                                                </div>
                                        </div>
                                                    
                                    </div>               
                                </div>
                                <div class="form-group">  
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Filter berdasarkan :</label>
                                                    <select class="form-control" name="filter2">
                                                        <option value ="Nama_Petani">Nama Petani</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Tahun_Panen">Tahun Panen</option>
                                                        <option value ="Bulan_Panen">Bulan Panen</option>
                                                        <option value ="Periode_Panen">Periode Panen</option>
                                                        <option value ="Nama_Produk">Nama Produk Panen</option>
                                                        <option value ="Luas_Lahan">Luas Lahan</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Filter</label>
                                                    <input class="form-control" name="isi2" placeholder=" " type="text">
                                                </div>
                                        </div>
                                                    
                                    </div>               
                                </div>
                                <div class="form-group">    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Filter berdasarkan :</label>
                                                    <select class="form-control" name="filter3">
                                                        <option value ="Nama_Petani">Nama Petani</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Tahun_Panen">Tahun Panen</option>
                                                        <option value ="Bulan_Panen">Bulan Panen</option>
                                                        <option value ="Periode_Panen">Periode Panen</option>
                                                        <option value ="Nama_Produk">Nama Produk Panen</option>
                                                        <option value ="Luas_Lahan">Luas Lahan</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Filter</label>
                                                    <input class="form-control" name="isi3" placeholder=" " type="text">
                                                </div>
                                        </div>
                                        <div class ="col-md-4">
                                            <div class = "row">
                                                <br>
                                                <div class ="col-md-2">  
                                                    <input class="btn btn-primary btn-md" type="submit" value="Filter"> 
                                                </div> 
                                            </div>             
                                        </div>            
                                    </div>               
                                </div>
                                       
                             </form>
                        <body>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Petani</th>
                                                        <th>Tanaman</th>
                                                        <th>Bulan Panen</th>
                                                        <th>Tahun Panen</th>
                                                        <th>Periode Tanam</th>
                                                        <th>Jumlah Hasil Panen (Kuintal)</th>
                                                        <th>Produk</th>
                                                        <th>Luas Lahan(M2)</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT Nama_Petani, Nama_Tanaman, Bulan_Panen, Tahun_Panen, Periode_Panen, Jumlah_Hasil_Panen, Nama_Produk, Luas_Lahan FROM trans_hasil_Panen inner join master_petani on trans_hasil_Panen.ID_Petani = master_petani.ID_User inner join master_spesies_tanaman on trans_hasil_panen.ID_Spesies = master_spesies_tanaman.ID_Spesies inner join master_produk_tani on trans_hasil_panen.ID_Produk = master_produk_tani.ID_Produk");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){   
                                                    ?>
                                               
                                                     <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['Nama_Petani']?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><?php echo $brs['Bulan_Panen']?></td>
                                                        <td><?php echo $brs['Tahun_Panen']?></td>
                                                        <td><?php echo $brs['Periode_Panen']?></td>
                                                        <td><?php echo $brs['Jumlah_Hasil_Panen']?></td>
                                                        <td><?php echo $brs['Nama_Produk']?></td>
                                                        <td><?php echo $brs['Luas_Lahan']?></td>
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--end konten-->
                     </div>
                </div>
            </div>
         </div>
          <script src="css/side_menu.js"></script>
        <footer class="navbar navbar-default navbar-fixed-bottom">
            <div class="container-fluid">
                <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
            </div>
        </footer>                         
</body></html>