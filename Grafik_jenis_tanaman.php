
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
        <style type="text/css">
        ${demo.css}
        </style>
         <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jenis Tanaman'
        },
        xAxis: {
            categories: [
                'Persawahan',
                'Perkebunan',
                'Perhutanan'
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Tanaman (Spesies)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">: </td>' +
                '<td style="padding:0">{point.y} Tanaman</td></tr>',
        },
            plotOptions: {
            column: {

                borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                }
            }, 
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            //window.open(this.options.url);
                            document.location = this.options.url;
                        }
                    }
                }
            }
        },
        
        legend: { symbolHeight: '0px' },
        series: [{
            name: ' Jenis Tanaman ',
            data: [{y:
            <?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_spesies_tanaman` WHERE Jenis_Tanaman='Persawahan'");
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#434348', url:'List_grafik_jenis_tanaman.php?jenis=Persawahan'} , 
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_spesies_tanaman` WHERE Jenis_Tanaman='Perkebunan'");
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#90ed7d',url:'List_grafik_jenis_tanaman.php?jenis=Perkebunan'} , 
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_spesies_tanaman` WHERE Jenis_Tanaman='Kehutanan'");
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#8085e9',url:'List_grafik_jenis_tanaman.php?jenis=Perhutanan'}]

        }]
    });
});
        </script>
    </head><body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
                </div>  
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
             <li><a href="login.php">Masuk</a></li>
                </ul>
                    </li>
                 </ul>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                                <div style="margin-bottom:15px;" align="right">
                                    <form action="cari_morfologi.php" method="post">
                                        <input type="text" name="input_cari_morfologi" placeholder="Cari Berdasar Nama Tanaman" style="width:250px;color:black;" />
                                        <input type="submit" name="cari" value="Cari" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                                    </form>
                                </div>
                            </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                  
                        <style>
                        <div.scroll {
                        width: 1080px;
                        overflow: auto;
                        }
                        </style>
                        <div class ="scroll">      
                        <script src="css/highcharts.js"></script>
                        <script src="css/highcharts/modules/exporting.js"></script>

                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>


                        <!--konten-->
                        </div>
                    </div>
                </div>
            </div>
            <script src="css/side_menu.js"></script>
            <!--end wraper-->

    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
    

</body></html>