<?php
//include("koneksi.php");
$provinsi =$_POST['provinsi'];
$Id_spesies = $_POST['tanaman'];
$tahun1 = $_POST['tahun1'];
$tahun2 = $_POST['tahun2'];
$tahun3 = $_POST['tahun3'];
$tahun4 = $_POST['tahun4'];
$tahun5 = $_POST['tahun5'];

?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar-tanaman.css" rel="stylesheet">
               <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Hasil Panen Pertahun di Wilayah <?php echo $provinsi?> '
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '<?php echo $tahun1; ?>',
                '<?php echo $tahun2; ?>',
                '<?php echo $tahun3; ?>',
                '<?php echo $tahun4; ?>',
                '<?php echo $tahun5; ?>']
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Jumlah Hasil Panen (kuintal)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true},
               borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                },
                enableMouseTracking: false
            }
        },
        legend: { symbolHeight: '0px' },
        series: [{
            name: 'Hasil Panen',
            data: [{y:
             <?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$Id_spesies' AND Provinsi= '$provinsi' AND Tahun_Panen = $tahun1");
                $jumlah = 0;
                ($panen = mysqli_fetch_array($hasil));
                $total = $panen['jumlah'];
                if ($total == null) {
                    echo '0';
                } else {
                    echo $panen['jumlah'];;
                }

            ?>, color: '#434348'} ,
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$Id_spesies' AND Provinsi= '$provinsi' AND Tahun_Panen = $tahun2");
                $jumlah = 0;
                ($panen = mysqli_fetch_array($hasil));
                $total = $panen['jumlah'];
                if ($total == null) {
                    echo '0';
                } else {
                    echo $panen['jumlah'];;
                }

            ?> , color: '#8085e9'} ,
            
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$Id_spesies' AND Provinsi= '$provinsi' AND Tahun_Panen = $tahun3");
                $jumlah = 0;
                ($panen = mysqli_fetch_array($hasil));
                $total = $panen['jumlah'];
                if ($total == null) {
                    echo '0';
                } else {
                    echo $panen['jumlah'];;
                }

            ?> , color: '#f15c80'} ,
          
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$Id_spesies' AND Provinsi= '$provinsi' AND Tahun_Panen = $tahun4");
                $jumlah = 0;
                ($panen = mysqli_fetch_array($hasil));
                $total = $panen['jumlah'];
                if ($total == null) {
                    echo '0';
                } else {
                    echo $panen['jumlah'];;
                }

            ?> , color: '#e4d254'} ,

             {y:<?php
                $koneksi = include("koneksi.php");          
                
                $hasil = mysqli_query($koneksi,"SELECT sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$Id_spesies' AND Provinsi= '$provinsi' AND Tahun_Panen = $tahun5");
                $jumlah = 0;
                ($panen = mysqli_fetch_array($hasil));
                $total = $panen['jumlah'];
                if ($total == null) {
                    echo '0';
                } else {
                    echo $panen['jumlah'];;
                }

            ?> , color: '#e4d254'} ,
            ]
        }]
    });
});
        </script>

    </head><body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    
     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-md-12">
                        <!---konten-->
                        <?php                                                         
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query= mysql_query("SELECT * FROM master_spesies_tanaman where ID_Spesies = '$Id_spesies';");
                            $brsnama = mysql_fetch_assoc($query);
                            ?>
                        <h2>Grafik Hasil Panen Tanaman <?php echo $brsnama['Nama_Tanaman']?> <?php echo $provinsi ?></h2>
                       <div class="col-md-2">
                            <a style="font-size:20px". onclick="window.history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</a>
                        </div>
                      <div style="margin-bottom:15px;" align="center">
                     
                        </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">     
                                <script src="css/highcharts.js"></script>
                                <script src="css/highcharts/modules/exporting.js"></script>
                                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                <div class="row">
                            <div class="col-md-8">  
                            </div>
                            <div class="col-md-4 text-left">
                                <a class="btn btn-lg btn-info" href="Summary_grafik_hasil_panen.php?tanaman=<?php echo $Id_spesies?>&provinsi=<?php echo $provinsi?>&tahun1=<?php echo $tahun1?>&tahun2=<?php echo $tahun2?>&tahun3=<?php echo $tahun3?>&tahun4=<?php echo $tahun4?>&tahun5=<?php echo $tahun5?>"> Summary</a>
                            </div>
                        </div>
                            

                            <div class="col-md-6">
                          
                        </div>
                        </div>
                    </div>
                        <!--konten-->
                    
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
</body></html>