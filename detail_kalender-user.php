<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "PET")
  {
    header("Location:login.php?pesan=hanya untuk user petani");
  } 
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
    </head>

    <body>
       <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Sistem Informasi Tanaman Pertanian</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
            $id_user = $_SESSION['ID_User'];                                       
            mysql_connect("localhost","root","") or die(mysql_error());
            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
            $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
            $brsnama = mysql_fetch_array($query)
            ?>
        <li><a href="#">HAI! <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li><a href="tutup_session.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> Menu Tani <?php echo $brsnama['Nama_Petani'] ?></a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                
                </div><!-- bs-example-navbar-collapse-1 -->
                  </ul>
                </div>
    </nav>

<div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="daftar_trans_aktivitas.php">Aktivitas Tani</a></li>
                        <li><a href="Daftar Hasil Panen.php">Hasil Panen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-user.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-user.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-user.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-user.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Grafik</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafikpanenpetani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <?php
                            $id_spesies = $_GET['id'];
                            $masa_tanam = $_GET['masa_tanam'];
                            $kabupaten = $_GET['kabupaten'];
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query = mysql_query("SELECT  Trans_kalender_tanam.ID_Spesies as ID_Spesies, Nama_Kalender, Masa_Tanam, Kabupaten, Provinsi, date_format(Tanggal_Awal, '%d-%m-%Y') as Tanggal_Awal, date_format(Tanggal_Akhir, '%d-%m-%Y') as Tanggal_Akhir, date_format(Pengolahan_Lahan_Awal, '%d-%m-%Y') as Pengolahan_Lahan_Awal, date_format(Pengolahan_Lahan_Akhir, '%d-%m-%Y') as Pengolahan_Lahan_Akhir, date_format(Persiapan_Lahan_Awal, '%d-%m-%Y') as Persiapan_Lahan_Awal, date_format(Masa_Penanaman_Awal, '%d-%m-%Y') as Masa_Penanaman_Awal, date_format(Masa_Pemupukan, '%d-%m-%Y') as Masa_Pemupukan, date_format(Masa_Panen, '%d-%m-%Y') as Masa_Panen FROM trans_kalender_tanam where ID_Spesies = '$id_spesies' AND Masa_Tanam='$masa_tanam' AND Kabupaten='$kabupaten'");

                            while($brs = mysql_fetch_assoc($query)){
                            $id_spesies_detail = $brs['ID_Spesies'];
                            ?>
                            <?php
                                 $querytanaman= mysql_query("SELECT * FROM master_spesies_tanaman Where ID_Spesies = '$id_spesies_detail'");
                                 $brstanaman = mysql_fetch_assoc($querytanaman);
                                ?> 
                                <h2><?php echo $brs['Nama_Kalender']?></h2>
                                <a style="font-size:20px" href="Daftar Kalender Tanam-user.php"><span class="glyphicon glyphicon-chevron-left"></span>Kembali ke Daftar Tanaman</a>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        Tanaman     
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brstanaman['Nama_Tanaman']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Nama Kalender  
                                    </div>   
                                    <div class="col-md-10">
                                        : <?php echo $brs['Nama_Kalender']
                                        ?>
                                    </div>
                                 </div>   
                                 <div class="row">
                                    <div class="col-md-2">
                                        Masa Tanam  
                                    </div>   
                                    <div class="col-md-10">
                                        : <?php echo $brs['Masa_Tanam']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                        Kabupaten
                                    </div>
                                    <div class="col-md-10">     
                                        : <?php echo $brs['Kabupaten']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                        Provinsi   
                                    </div>  
                                    <div class="col-md-10">
                                        : <?php echo $brs['Provinsi']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2"> 
                                        Tanggal Awal
                                    </div>     
                                    <div class="col-md-10"> 
                                        : <?php echo $brs['Tanggal_Awal']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Tanggal Akhir
                                    </div>     
                                    <div class="col-md-10">
                                        : <?php echo $brs['Tanggal_Akhir']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Pengolahan Lahan Awal
                                    </div>    
                                    <div class="col-md-10">
                                        : <?php echo $brs['Pengolahan_Lahan_Awal']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Pengolahan Lahan Akhir
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Pengolahan_Lahan_Akhir']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Persiapan Lahan Awal
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Persiapan_Lahan_Awal']
                                        ?>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-2">
                                        Masa Penanaman Awal
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Masa_Penanaman_Awal']
                                        ?>
                                    </div>
                                 </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        Masa Pemupukan 
                                    </div> 
                                    <div class="col-md-10">    
                                        : <?php echo $brs['Masa_Pemupukan']
                                        ?>
                                    </div>
                                 </div>
                                       <div class="row">
                                    <div class="col-md-2">
                                        Masa Panen  
                                        </div>  
                                        <div class="col-md-10">
                                        : <?php echo $brs['Masa_Panen']
                                        ?>
                                    </div>
                                 </div>                                

                            </div>   
                        </div>  


                            <?php
                            }
                            ?>

                            <!--end konten-->
    
                     </div>
                </div>
            </div>
         </div>
         <script src="css/side_menu.js"></script>
         <footer class="navbar navbar-default navbar-fixed-bottom">
             <div class="container-fluid">
                <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
                </div>
            </footer>
    </body>
</html>