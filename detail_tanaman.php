<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
    </head>

    <body>
       <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Sistem Informasi Tanaman Pertanian</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">HAI! <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li><a href="#">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                
                </div><!-- bs-example-navbar-collapse-1 -->
                  </ul>
                </div>
    </nav>

 <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Tanaman</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="daftar_morfologi.php">Morfologi Tanaman</a></li>
                        <li><a href="Daftar Tanaman.php">Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam.php">Kalender Tanam</a></li>
                        <li><a href="Daftar Peta Lahan.php">Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Data Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Aktivitas.php">Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php">Aktivitas Tanaman</a></li>
                
                    </ul>
                </li>
               
            </ul>
        </div><!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <?php
                            $id_spesies = $_GET['Id_spesies'];
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query = mysql_query("SELECT * FROM master_spesies_tanaman where ID_Spesies = '$id_spesies'");

                            while($brs = mysql_fetch_assoc($query)){
                            ?>
                            <div class="row">
                                <div class="col-md-8">
                                <h1>Detail Tanaman <?php echo $brs['Nama_Tanaman']?></h1>
                                <a style="font-size:20px" href="Daftar Tanaman.php"><span class="glyphicon glyphicon-chevron-left"></span>Kembali ke Daftar Tanaman</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-2">

                                    <?php echo '<img src="images/foto1/'.$brs['Foto1'].'"class="img-rounded" height="140" width="140"/>'
                                    ?>
                                </div>
                                 <div class="col-md-2">

                                    <?php echo '<img src="images/foto2/'.$brs['Foto2'].'"class="img-rounded" height="140" width="140"/>'
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Nama Tanaman    
                                            </div>
                                            <div class="col-md-6">
                                            : <?php echo $brs['Nama_Tanaman']
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                             <div class="col-md-6">
                                            Jenis Tanaman
                                             </div>  
                                            <div class="col-md-6"> 
                                            : <?php echo $brs['Jenis_Tanaman']
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Nama Latin
                                            </div>
                                            <div class="col-md-6">
                                            : <?php echo $brs['Nama_Latin']
                                            ?>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Habitat
                                            </div>
                                            <div class="col-md-6">      
                                            : <?php echo $brs['Habitat']
                                            ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Masa Tanam
                                            </div>
                                            <div class="col-md-6">     
                                            : <?php echo $brs['Masa_Tanam']
                                            ?> Hari
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                

                                

                                




                                <?php
                                $id_morfologi = $brs['ID_Morfologi'];
                                 $querymorfologi= mysql_query("SELECT * FROM master_morf_tanaman Where ID_Morfologi = '$id_morfologi'");
                                 $brsmorfologi = mysql_fetch_assoc($querymorfologi);
                                ?> 
                                 


                              


                            </div> 
                              <br>
                             <div class="row">
                                    <div class="col-md-10">

                                    <div class="row">
                                    <div class="col-md-2">
                                       
                                                Akar
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Akar']?>
                                            
                                               
                                    </div>
                                       </div>           
                             <div class="row">
                                    <div class="col-md-2">
                                       
                                                Batang
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Batang']?>
                                            
                                               
                                    </div>
                                       </div>     
                                <div class="row">
                                    <div class="col-md-2">
                                       
                                                Daun
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Daun']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Buah
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Buah']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Biji
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Biji']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Perkembangbiakan
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Perkembangbiakan']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Iklim
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Iklim']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Jenis Tanah
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Jenis_Tanah']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Kelembababan
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Kelembaban']?>
                                            
                                               
                                    </div>
                                       </div>
                                        <div class="row">
                                    <div class="col-md-2">
                                       
                                                Morfologi
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                  : <?php echo $brsmorfologi['Nama_Morfologi_Tanaman']
                                        ?>
                                            
                                               
                                    </div>
                                       </div>    
                                            </div>

                                <div class="col-md-2">
                                      
                                         
                                     
                                </div>

                                 </div> 
                              

                                    </div>
                                 
                        </div>  


                            <?php
                            }
                            ?>

                            <!--end konten-->
    
                     </div>
                </div>
            </div>
         </div>
         <script src="css/side_menu.js"></script>
         <footer class="navbar navbar-default navbar-fixed-bottom">
             <div class="container-fluid">
                <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
                </div>
            </footer>
    </body>
</html>